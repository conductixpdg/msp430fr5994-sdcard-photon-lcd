################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
radio_drv/cc1101_drv/cc1101_drv.obj: ../radio_drv/cc1101_drv/cc1101_drv.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/msp430_15.12.3.LTS/bin/cl430" -vmspx --data_model=restricted --use_hw_mpy=F5 --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/Users/hau.tran/Documents/Embedded101/Sd_Card_Photon/hal_mcu" --include_path="C:/Users/hau.tran/Documents/Embedded101/Sd_Card_Photon/radio_drv/cc1101_drv" --include_path="C:/Users/hau.tran/Documents/Embedded101/Sd_Card_Photon/radio_drv" --include_path="C:/Users/hau.tran/Documents/Embedded101/Sd_Card_Photon/SDCardLib" --include_path="C:/Users/hau.tran/Documents/Embedded101/Sd_Card_Photon/FatFs" --include_path="C:/Users/hau.tran/Documents/Embedded101/Sd_Card_Photon/HAL" --include_path="C:/Users/hau.tran/Documents/Embedded101/Sd_Card_Photon/driverlib/MSP430FR5xx_6xx" --include_path="C:/ti/ccsv6/tools/compiler/msp430_15.12.3.LTS/include" --advice:power="all" --advice:hw_config=all -g --define=__MSP430FR5994__ --define=_MPU_ENABLE --diag_warning=225 --diag_wrap=off --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="radio_drv/cc1101_drv/cc1101_drv.d" --obj_directory="radio_drv/cc1101_drv" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

radio_drv/cc1101_drv/cc1101_utils.obj: ../radio_drv/cc1101_drv/cc1101_utils.c $(GEN_OPTS) | $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: MSP430 Compiler'
	"C:/ti/ccsv6/tools/compiler/msp430_15.12.3.LTS/bin/cl430" -vmspx --data_model=restricted --use_hw_mpy=F5 --include_path="C:/ti/ccsv6/ccs_base/msp430/include" --include_path="C:/Users/hau.tran/Documents/Embedded101/Sd_Card_Photon/hal_mcu" --include_path="C:/Users/hau.tran/Documents/Embedded101/Sd_Card_Photon/radio_drv/cc1101_drv" --include_path="C:/Users/hau.tran/Documents/Embedded101/Sd_Card_Photon/radio_drv" --include_path="C:/Users/hau.tran/Documents/Embedded101/Sd_Card_Photon/SDCardLib" --include_path="C:/Users/hau.tran/Documents/Embedded101/Sd_Card_Photon/FatFs" --include_path="C:/Users/hau.tran/Documents/Embedded101/Sd_Card_Photon/HAL" --include_path="C:/Users/hau.tran/Documents/Embedded101/Sd_Card_Photon/driverlib/MSP430FR5xx_6xx" --include_path="C:/ti/ccsv6/tools/compiler/msp430_15.12.3.LTS/include" --advice:power="all" --advice:hw_config=all -g --define=__MSP430FR5994__ --define=_MPU_ENABLE --diag_warning=225 --diag_wrap=off --display_error_number --silicon_errata=CPU21 --silicon_errata=CPU22 --silicon_errata=CPU40 --printf_support=minimal --preproc_with_compile --preproc_dependency="radio_drv/cc1101_drv/cc1101_utils.d" --obj_directory="radio_drv/cc1101_drv" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


