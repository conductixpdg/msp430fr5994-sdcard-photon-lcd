#include <msp430.h>
#include <stdint.h>
#include "driverlib.h"
#include <ff.h>
#include <diskio.h>
#include "HAL_SDCard.h"
#include "cc1101_def.h"
#include "radio_drv.h"
#include "cc1x_utils.h"
#include "hal_spi_rf.h"
#include "hal_timer.h"

#define TX_BUF_SIZE 24
#define FREQUENCY	902750
#define CHECK_MSEC_TA 10			// Interval of timer A count to check button state
#define CHECK_MSEC_TB 35			// Interval of timer A count to check button state
#define PRESS_MSEC 10			// stable time before registering pressed
#define RELEASE_MSEC 100		// stable time before registering release
/*---------------- Global Variables ---------------------*/
volatile char TXData;
FIL file;                                               /* Opened file object */
FATFS fatfs;                                            /* File system object */
DIRS dir;                                               /* Directory object   */
FRESULT errCode;                                        /* Error code object  */
FRESULT res;                                            /* Result object      */
UINT bytesRead;                                         /* Bytes read object  */
UINT read;                                              /* Read bytes object  */
Calendar calendar;
FRESULT WriteFile(char*, char*, WORD);
unsigned char MST_Data,SLV_Data;
BYTE buffer[32];
int result=1;
volatile unsigned char buttonPressed;
volatile unsigned char currentState = 0;
volatile unsigned char count;
unsigned char check_rf_end_packet = 0;
/******************************************************************************
* LOCAL VARIABLES */

/*---------------- Function Called ----------------------*/

void Init_GPIO(void);
void Init_I2C(void);
void Init_CLK(void);
void Init_FAT(void);
void Init_RTC(void);
void Init_TimerA(void);
void Init_TimerB(void);
void Log_SDCard(char data);
void Init_Radio(void);
void Init_SPI_Radio(void);

int main(void)
{

	WDT_A_hold(WDT_A_BASE);			// Stop WatchDog Timer
	Init_GPIO();					// Initialization GPIO
	Init_CLK();						// Initialization system clocks
	//Init_SPI_Radio();				// Initialization SPI communication for Radio Communication
	Init_Radio();					// Initialization Radio Module
	Init_TimerA();					// Initialization TimerA
	Init_TimerB();
	//Init_RTC();					// Initialize real time will cause problem with polling I2C Photon data request
	Init_FAT();						// Initialization SD Card
	Init_I2C();						// Initialization I2C communication with Photon Board
	__bis_SR_register(LPM0_bits | GIE);
	_no_operation();

}
/*---------------------------------------------------------------------------------
 * --------------------------- Initialization SPI ---------------------------
 * --------------------------------------------------------------------------------
 */
void Init_SPI_Radio(void) {
	// Configure USCI_B1 for SPI operation
	UCB1CTLW0 = UCSWRST;                    // **Put state machine in reset**
	UCB1CTLW0 |= UCSYNC | UCCKPL | UCMSB;   // 3-pin, 8-bit SPI slave
	// Clock polarity high, MSB
	UCB1CTLW0 |= UCSSEL__SMCLK;             // SMCLK
	UCB1BRW = 0x02;                         // /2
	//UCB1MCTLW = 0;                        // No modulation
	UCB1CTLW0 &= ~UCSWRST;                  // **Initialize USCI state machine**
	UCB1IE |= UCRXIE;                       // Enable USCI_B1 RX interrupt
}


/*---------------------------------------------------------------------------------
 * --------------------------- Initialization Radio Register ---------------------------
 * --------------------------------------------------------------------------------
 */
void Init_Radio(void) {
	radio_init(3);

	radio_set_freq(FREQUENCY);

	set_rf_packet_length(TX_BUF_SIZE);

	radio_receive_on();

	check_rf_end_packet = radio_pending_packet();
}



/*---------------------------------------------------------------------------------
 * --------------------------- Initialization Timer --------------------------
 * --------------------------------------------------------------------------------
 */
void Init_TimerA(void) {
	//Start timer in continuous mode sourced by SMCLK
	TA0CCTL0 = CCIE;                        						// TACCR0 interrupt enabled
	TA0CCR0 = (CHECK_MSEC_TA * 32768 ) / 1000;							// Counting time
	TA0CTL = TASSEL__ACLK | MC__UP; 								// ACLK, up_down mode
	if (check_rf_end_packet) {
		P2OUT ^= BIT6;
	}
}

void Init_TimerB(void) {
	//Start timer in continuous mode sourced by SMCLK
	TB0CCTL0 = CCIE;                        						// TACCR0 interrupt enabled
	TB0CCR0 = (CHECK_MSEC_TB * 32768 ) / 1000;							// Counting time
	TB0CTL = TASSEL__ACLK | MC__UP; 								// ACLK, up_down mode
}
//******************************************************************************
//
//This is the Timer B0 interrupt vector service routine.
//
//******************************************************************************
#pragma vector=TIMER0_B0_VECTOR
__interrupt void TIMER0_B0_ISR (void)
{
	if(UCB2TXBUF == TXData){
		P1OUT ^= BIT0;
	}
	if(TA0CCTL0 |= CCIFG){
		P1OUT ^= BIT1;
	}
	if (FR_OK){
		P2OUT ^= BIT6;
	}
	__bic_SR_register(LPM0_bits);

}

//******************************************************************************
//
//This is the Timer A0 interrupt vector service routine.
//
//******************************************************************************
#pragma vector=TIMER0_A0_VECTOR
__interrupt void TIMER0_A0_ISR (void)
{
	TA0CCTL0 &= ~CCIFG;

	volatile unsigned char buttonState = 0x0E;
	currentState = (P3IN & 0x0E);
	if (currentState != buttonState){
		count++;
		if (count >= 4){			// <-- fix this
			buttonState = currentState;
			if (currentState != 0){
				buttonPressed = 1;
			}
			count = 0;
		}
	} else {
		count = 0;
		buttonPressed = 0;
	}
	__bic_SR_register(LPM0_bits);
	//P1OUT ^= BIT1;
}

/*---------------------------------------------------------------------------------
 * --------------------------- Print Variable to SD Card --------------------------
 * --------------------------------------------------------------------------------
 */
void Log_SDCard(char data){
	//f_mount(0, &fatfs);                       	//mount drive number 0
	//f_opendir(&dir, "/");				    		//root directory
	//res = f_open(&file, "/LogFile.txt", FA_OPEN_EXISTING | FA_WRITE);
	/* Move to end of the file to append data */
	res = f_lseek(&file, f_size(&file));
	f_printf(&file, "Button %c is PRESSED\n", data);
	f_close(&file);
	f_mount(0,0);
	P2OUT ^= BIT6;
	P2OUT ^= BIT6;
}



/*---------------------------------------------------------------------------------
 * --------------------------- Initialization FAT ---------------------------------
 * --------------------------------------------------------------------------------
 */

void Init_FAT(void){
	int errCode = -1;

	while (errCode != FR_OK){                               //go until f_open returns FR_OK (function successful)
		errCode = f_mount(0, &fatfs);                       //mount drive number 0
		errCode = f_opendir(&dir, "/");				    	//root directory
		errCode = f_open(&file, "/LogFile.txt", FA_OPEN_ALWAYS | FA_WRITE);
		if(errCode != FR_OK) result=0;                    	//used as a debugging flag
		if(errCode == 3) {
			P2OUT ^= BIT6;
		}
	}
	P2OUT |= BIT5;
}


/*---------------------------------------------------------------------------------
 * --------------------------- Initialization GPIO --------------------------------
 * --------------------------------------------------------------------------------
 */

void Init_GPIO(void){
	// Configure GPIO
	P7SEL0 |= BIT0 | BIT1;
	P7SEL1 &= ~(BIT0 | BIT1);

	// Configure Button RGY and LED

	P3DIR = ~(BIT1 + BIT2 + BIT3);		// Set input P3.1,2,3
	P3OUT |= BIT1 + BIT2 + BIT3;		// Set pull-up resistor
	P3REN |= BIT1 + BIT2 + BIT3;		// Enable internal resistor at P3.1,2,3
	P3IE |= BIT1 + BIT2 + BIT3;			// P3.1,2,3 Interrupt Enable
	P3IES &= ~(BIT1 + BIT2 + BIT3);		// P3.1,2,3 Hi/Lo edge
	P3IFG &= ~(BIT1 + BIT2 + BIT3);		// Clear Flag

	P1DIR |= BIT1 + BIT0;
	P1OUT = 0x00;
	P2DIR |= BIT5 + BIT6;
	P2OUT = 0x00;

	P5SEL1 &= ~(BIT0 | BIT1 | BIT2);        // USCI_B1 SCLK, MOSI, and MISO pin
	P5SEL0 |= (BIT0 | BIT1 | BIT2);
	// Set PJ.4 and PJ.5 as Primary Module Function Input, LFXT.
	GPIO_setAsPeripheralModuleFunctionInputPin(
			GPIO_PORT_PJ,
			GPIO_PIN4 + GPIO_PIN5,
			GPIO_PRIMARY_MODULE_FUNCTION
	);

	// Disable the GPIO power-on default high-impedance mode to activate
	// previously configured port settings
	PM5CTL0 &= ~LOCKLPM5;				// or PMM_unlockLPM5;
}


/*---------------------------------------------------------------------------------
 * --------------------------- Initialization I2C ---------------------------------
 * --------------------------------------------------------------------------------
 */

void Init_I2C(void) {
	/* Configure USCI_B2 for I2C mode */
	UCB2CTLW0 = UCSWRST;                    // Software reset enabled
	UCB2CTLW0 |= UCMODE_3 | UCSYNC;         // I2C mode, sync mode
	UCB2I2COA0 = 0x48 | UCOAEN;             // own address is 0x48 + enable
	UCB2CTLW0 &= ~UCSWRST;                  // clear reset register
	UCB2IE |= UCTXIE0 | UCSTPIE;            // transmit,stop interrupt enable

}

/*---------------------------------------------------------------------------------
 * --------------------------- Initialization Clock ---------------------------------
 * --------------------------------------------------------------------------------
 */

void Init_CLK(void) {
	// Set DCO frequency to 8 MHz
	CS_setDCOFreq(CS_DCORSEL_0, CS_DCOFSEL_6);
	//Set external clock frequency to 32.768 KHz
	CS_setExternalClockSource(32768, 0);
	//Set ACLK=LFXT
	CS_initClockSignal(CS_ACLK, CS_LFXTCLK_SELECT, CS_CLOCK_DIVIDER_1);
	// Set SMCLK = DCO with frequency divider of 1
	CS_initClockSignal(CS_SMCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
	// Set MCLK = DCO with frequency divider of 1
	CS_initClockSignal(CS_MCLK, CS_DCOCLK_SELECT, CS_CLOCK_DIVIDER_1);
	//Start XT1 with no time out
	CS_turnOnLFXT(CS_LFXT_DRIVE_3);
}

/*---------------------------------------------------------------------------------
 * --------------------------- I2C Transmission - Slave ISR -----------------------
 * --------------------------------------------------------------------------------
 */

#pragma vector = EUSCI_B2_VECTOR
__interrupt void USCI_B2_ISR(void)
{

	switch(__even_in_range(UCB2IV, USCI_I2C_UCBIT9IFG))
	{
	case USCI_NONE:          break;     // Vector 0: No interrupts
	case USCI_I2C_UCALIFG:   break;     // Vector 2: ALIFG
	case USCI_I2C_UCNACKIFG: break;     // Vector 4: NACKIFG
	case USCI_I2C_UCSTTIFG:  break;     // Vector 6: STTIFG
	case USCI_I2C_UCSTPIFG:             // Vector 8: STPIFG
		TXData = 0;
		UCB2IFG &= ~UCSTPIFG;           // Clear stop condition int flag
		break;
	case USCI_I2C_UCRXIFG3:  break;     // Vector 10: RXIFG3
	case USCI_I2C_UCTXIFG3:  break;     // Vector 12: TXIFG3
	case USCI_I2C_UCRXIFG2:  break;     // Vector 14: RXIFG2
	case USCI_I2C_UCTXIFG2:  break;     // Vector 16: TXIFG2
	case USCI_I2C_UCRXIFG1:  break;     // Vector 18: RXIFG1
	case USCI_I2C_UCTXIFG1:  break;     // Vector 20: TXIFG1
	case USCI_I2C_UCRXIFG0:  break;     // Vector 22: RXIFG0
	case USCI_I2C_UCTXIFG0:             // Vector 24: TXIFG0
		UCB2TXBUF = TXData;
		break;
	case USCI_I2C_UCBCNTIFG: break;     // Vector 26: BCNTIFG
	case USCI_I2C_UCCLTOIFG: break;     // Vector 28: clock low timeout
	case USCI_I2C_UCBIT9IFG: break;     // Vector 30: 9th bit
	default: break;
	}
}

/*---------------------------------------------------------------------------------
 * ---------------------------Port 4 interrupt service routine --------------------
 * --------------------------------------------------------------------------------
 */


#pragma vector=PORT3_VECTOR
__interrupt void Port_3(void)
{
	if(buttonPressed){
		buttonPressed = 0;
		if(P3IFG & BIT3){
			TXData = 'R';
			//P2OUT ^= BIT5;                            // P1.1 = toggle
			P3IFG &= ~BIT3;                           // P3.3 IFG cleared
		}
		if(P3IFG & BIT2){
			TXData = 'G';
			//P2OUT ^= BIT5;                            // P1.1 = toggle
			P3IFG &= ~BIT2;                           // P3.2 IFG cleared
		}
		if(P3IFG & BIT1){
			TXData = 'Y';
			//P2OUT ^= BIT5;                            // P1.1 = toggle
			P3IFG &= ~BIT1;                           // P3.1 IFG cleared
		}
		res = f_lseek(&file, f_size(&file));
		f_printf(&file, "Button %c is PRESSED\n", TXData);
		f_sync(&file);
	}
	P3IFG &= ~(BIT3 + BIT2 + BIT1);
}

/*---------------------------------------------------------------------------------
 * --------------------------- Radio SPI Interrupt ---------------------------------
 * --------------------------------------------------------------------------------
 */

#pragma vector=EUSCI_B1_VECTOR
__interrupt void USCI_B1_ISR(void)
{
	while (!(UCB1IFG&UCTXIFG));             // USCI_B1 TX buffer ready?
	UCB1TXBUF = UCB1RXBUF;                  // Echo received data
}





/*
 * Real Time Clock Initialization
 */
void Init_RTC()
{
	//Setup Current Time for Calendar
	calendar.Seconds    = 0x55;
	calendar.Minutes    = 0x30;
	calendar.Hours      = 0x04;
	calendar.DayOfWeek  = 0x01;
	calendar.DayOfMonth = 0x11;
	calendar.Month      = 0x09;
	calendar.Year       = 0x2017;

	// Initialize RTC with the specified Calendar above
	RTC_C_initCalendar(RTC_C_BASE,
			&calendar,
			RTC_C_FORMAT_BCD);

	RTC_C_setCalendarEvent(RTC_C_BASE,
			RTC_C_CALENDAREVENT_MINUTECHANGE
	);

	RTC_C_clearInterrupt(RTC_C_BASE,
			RTC_C_TIME_EVENT_INTERRUPT
	);

	RTC_C_enableInterrupt(RTC_C_BASE,
			RTC_C_TIME_EVENT_INTERRUPT
	);

	//Start RTC Clock
	RTC_C_startClock(RTC_C_BASE);
}
